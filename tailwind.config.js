module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ['Open Sans', 'Arial', 'sans-serif']
    },
    extend: {      
      spacing: {
      '2/3': '66.666667%'
      }
    },
  },
  variants: {
    extend: {
    },
  },
  plugins: [],
}
