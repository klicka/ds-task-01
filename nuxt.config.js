export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'ds-task-01',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  target: 'static',

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~plugins/leaflet.js', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/apollo'
  ],

  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: "http://www.klicka.eu/graphql"
      }
    }
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    'vue-scrollto/nuxt',
  ],
  axios: {
    baseURL: process.env.BASE_URL || 'https://ds-db-01-default-rtdb.europe-west1.firebasedatabase.app',
    credentials: false
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  env: {
    baseUrl: process.env.BASE_URL || 'https://ds-db-01-default-rtdb.europe-west1.firebasedatabase.app',
    fbAPIKey: 'AIzaSyCfcci6kmoo8vNIHXgKwXYqzSx4Ijeanis',
    NETLIFY_AUTH_TOKEN: '5yKCfAbZ5ye2MCk95jy4WH3PVCAQ3AOFcm1QdA1P7LA'
  },
}
